#!/usr/bin/env bash
#File as variable
# file=$1
file=$1

awk '{ req=$6; link=$7; size=$10; code=$9; if(code >= 500 && code < 600) print substr(req, 2),link,size }' $file | sort -urnk3 > "5xx.txt"
awk '{ req=$6; link=$7; size=$10; code=$9; if(code >= 400 && code < 500) print substr(req, 2),link,size }' $file | sort -urnk3 > "4xx.txt"
awk '{ req=$6; link=$7; size=$10; code=$9; if(code >= 300 && code < 400) print substr(req, 2),link,size }' $file | sort -urnk3 > "3xx.txt"
awk '{ req=$6; link=$7; size=$10; code=$9; if(code >= 200 && code < 300) print substr(req, 2),link,size }' $file | sort -urnk3 > "2xx.txt"

# awk '{ req=$6; link=$7; size=$10; code=$9;
# if(code >= 500 && code < 600) print req,link,size,code > "5xx.txt";
# else if(code >= 400 && code < 500) print req,link,size,code > "4xx.txt";
# else if(code >= 300 && code < 400) print req,link,size,code > "3xx.txt";
# else if(code >= 200 && code < 300) print req,link,size,code > "2xx.txt"}' $file | sort -urnk3 "5xx.txt" -o "5xx.txt"
