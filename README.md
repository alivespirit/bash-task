Develop a shell script that will parse access and error logs in default apache/nginx format and build 4 files based on http response code. Output files should be `2xx.txt`, `3xx.txt`, `4xx.txt`, `5xx.txt` and should contain list of requested endpoints and size in bytes. After the files are built, content should be sorted by `size in bytes` field and contain only unique records.  
Example of file `2xx.txt` content after creation:  
```
GET /api/admin 3874
GET /user 1624
POST /auth 6721
GET /api/admin 3874
PUT /files/1.txt 8254
GET /user 1624
```
Example of final sorted file:
```
PUT /files/1.txt 8254
POST /auth 6721
GET /api/admin 3874
GET /user 1624
```
  
Script should be placed in the root directory of repository and should pass build process.  
  
Testing the script:  
```
git checkout -b testbranch
git add *
git commit -m "Script testing"
git push origin testbranch
```

Useful resources:  
https://linuxconfig.org/bash-scripting-tutorial  
https://linuxize.com/post/bash-functions/  
https://www.cyberciti.biz/tips/bash-shell-parameter-substitution-2.html  
https://www.cyberciti.biz/tips/  
https://explainshell.com/  
