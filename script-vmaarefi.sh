#!/bin/bash
file=$1
awk '{request=$6; path=$7; size=$10; status_code=$9;
if(status_code ~ /2[0-9][0-9]$/) print substr(request, 2),path,size > "2xx.txt";
else if(status_code ~ /3[0-9][0-9]$/) print substr(request, 2),path,size > "3xx.txt";
else if(status_code ~ /4[0-9][0-9]$/) print substr(request, 2),path,size > "4xx.txt";
else if(status_code ~ /5[0-9][0-9]$/) print substr(request, 2),path,size > "5xx.txt";
}' $file
  
for var in *.txt; do
  if [ -f $var ]
  then
    sort -urnk3 $var -o $var
    echo "File $var sorted";    
  else
    echo "No such .txt files found"
  fi
done
